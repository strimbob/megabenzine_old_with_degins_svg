import React from 'react';
 import StoryCom from './storyCom.js'
export default class Story  extends React.Component{

  render(){
    return(
      <div>
      <StoryCom className={this.props.className} img={this.props.data.imgtwo}
        textClass={this.props.textclass}
         headline={this.props.data.headline} tag={this.props.data.tag} />
      </div>
    );
  }
}
