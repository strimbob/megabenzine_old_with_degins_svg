import phoneSvg from '../../static/menuBar/menuBarPhone.svg'
import desktopSvg from '../../static/menuBar/menuBarDtop.svg'

const svg =[
{phone : phoneSvg,
desktop:  desktopSvg
}];
export default svg;
