import React from 'react';

import MenuBar from './subComp/menuBar.js'
import Footer from './subComp/footer.js'
import Logo from './subComp/logo.js'

import menuTitles from './data/titles/menubar.js'
import spotLight from './data/titles/spotLight.js'
import stuffToDo from './data/titles/stuffToDo.js'
import thingToEat from './data/titles/thingToEat.js'
import placeToGo from './data/titles/placeToGo.js'
import muisc from './data/titles/muisc.js'

import config from './firebase/configFireBase.js'
import FrontPage from './frontPage/frontPage.js'

export default class App extends React.Component {

  render() {

    console.log(this.props.location.pathname);
    console.log("this.props.location.pathname");

    return (
      <div>
        <div className='_logo'>
          <Logo/>
        </div>
        <div className='_MenuBar'>
          <MenuBar menuTitles={menuTitles} address={this.props}/>
        </div>
        <div className="spotLight">
          <FrontPage data={spotLight}/>
        </div>
        <div className='StuffToDo '>
          <FrontPage data={stuffToDo} link={menuTitles[0].link}/>
        </div>
        <div className='ThingToEat'>
          <FrontPage data={thingToEat} link={menuTitles[1].link}/>
        </div>
        <div className='PlaceToGo '>
          <FrontPage data={placeToGo} link={menuTitles[2].link}/>
        </div>
        <div className='Muisc'>
          <FrontPage data={muisc} link={menuTitles[3].link}/>
        </div>
        <Footer/>
      </div>
    );
  }
}
