import React from 'react';

import MenuBar from './subComp/menuBar.js'
import Footer from './subComp/footer.js'
import Logo from './subComp//logo.js'

import thingsToDoSVG from './data/background/thingToDoSVG.js'
import menuTitles from './data/titles/menubar.js'
import NameForm from './subComp/nameForm.js'
export default class UploadStory extends React.Component {

  render() {
    return (

<div>
  <div className='_logo'>
    <Logo/>
  </div>
  <div className='_MenuBar'>
    <MenuBar menuTitles={menuTitles}/>
  </div>
      <NameForm />

</div>


    );
  }
}
