import React from 'react';
import MediaQuery from 'react-responsive';
import media from '../../scss/mediaRect.js';
import MediaQueriesImg from './MediaQueriesImg.js';
export default class Story extends React.Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }
  handleClick() {
    let address = this.props.story.db+'/'+this.props.story.headline;
    let res = address.split("/");
    let addresBar = res[1]+'?';
    for(var q =2;q<res.length;q++){
        addresBar += res[q];
    }
     window.open(addresBar,"_self");
  }
  componentWillMount() {
  let addresses = this.props.classHeading;
console.log(addresses);
}
  render() {
    return (
      <div className={this.props.data} onClick={this.handleClick}>
        <div className={this.props.story.classHeading + ' ' + this.props.color} >{this.props.story.headline}</div>
        <MediaQueriesImg story={this.props.story}/>
        <div className={this.props.story.tagClass  + ' ' + this.props.color}>{this.props.story.tag}</div>
      </div>
    );
  }
}
