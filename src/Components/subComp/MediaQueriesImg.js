import React from 'react';
import MediaQuery from 'react-responsive';
import media from '../../scss/mediaRect.js';

export default class MediaQueriesImg  extends React.Component{
  render(){
    return(
    <div>
      <MediaQuery minWidth={media.desktop+1}>
        <img  src= {this.props.story.imgDtop} alt='\|/' />
      </MediaQuery>
      <MediaQuery  maxWidth={media.desktop}>
        <img src= {this.props.story.imgPhone} alt='\|/' />
      </MediaQuery>
    </div>
        );
      }
    }
