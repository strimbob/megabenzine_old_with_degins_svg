import React from 'react';

export default class Footer  extends React.Component{
  render(){
    return(
      <div className ='footer'>
        <div className ='footer__text'>
          <p>A digital zine </p>
          <p>telling the story of leiden and the surrounding area</p>
        </div>
        <div className ='uploadButton'> <a href='/upload' > upload</a></div>
        </div>
        );
      }
    }
