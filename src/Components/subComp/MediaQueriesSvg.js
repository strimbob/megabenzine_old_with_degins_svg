import React from 'react';
import MediaQuery from 'react-responsive';
import media from '../../scss/mediaRect.js';

export default class MediaQueriesSvg extends React.Component {
  render() {
  
    return (
      <div>
        <MediaQuery minWidth={media.desktop + 1}>
          <this.props.svg.svgDtop/>
        </MediaQuery>
        <MediaQuery maxWidth={media.desktop}>
          <this.props.svg.svgPhone/>
        </MediaQuery>
      </div>
    );
  }
}
