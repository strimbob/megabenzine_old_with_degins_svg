import React from 'react';
import MediaQueriesSvg from '../subComp/MediaQueriesSvg.js'
import Story from '../subComp/storyCom.js'
import {getStoryFireBase} from '../firebase/fireBaseReqest.js'
export default class FrontPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {storyArray: []};
  }
  componentWillMount() {
    getStoryFireBase(this.props.data.db).then(_story => {
      var allStory = [];
      for (var q = 0; q < this.props.data.classNames.length; q++) {
        allStory.push(_story[Object.keys(_story)[q]]);
        allStory[q].classNames = this.props.data.classNames[q]._class;
        allStory[q].color = this.props.data.color;
        allStory[q].db = this.props.data.db;
        allStory[q].classHeading = "story__heading";
        allStory[q].tagClass  = 'story__tag ';
      }
      this.setState({storyArray: allStory})
    });
  }
  render() {
    return (
      <div>
        <div ><a className={this.props.data.classNameTitle} href={this.props.link}> {this.props.data.title}</a></div>
        {this.state.storyArray.map(function(storyArray, index) {
          return <Story data={storyArray.classNames} story={storyArray} color={storyArray.color} key={index}/>})}
        <MediaQueriesSvg svg={this.props.data}/>
      </div>
    );
  }
}
