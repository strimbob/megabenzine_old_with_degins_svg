import React from 'react';
import Story from '../subComp/storyCom.js'
import {getStoryFireBase} from '../firebase/fireBaseReqest.js'
export default class SubIndexPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {storyArray: []};
  }
  componentWillMount() {
    getStoryFireBase(this.props.data.db).then(_story => {
      var allStory = [];
      for (var q = 0; q < Object.keys(_story).length; q++) {
        allStory.push(_story[Object.keys(_story)[q]]);
        allStory[q].classNames = 'storySub';
        allStory[q].color = this.props.data.color;
        allStory[q].db = this.props.data.db;
        allStory[q].classHeading = "storySub__heading ";
        allStory[q].tagClass  = 'storySub__tag ';
      }
      this.setState({storyArray: allStory})
    });
  }
  render() {
    return (
      <div>
      {this.state.storyArray.map(function(storyArray, index) {
        return <Story data={storyArray.classNames} story={storyArray} color={storyArray.color} key={index}/>})}
      </div>
    );
  }
}


// {this.state.storyArray.map(function(storyArray, index) {
//   return <StoryComSub data={storyArray.classNames} story={storyArray} color={storyArray.color} key={index}/>})}
