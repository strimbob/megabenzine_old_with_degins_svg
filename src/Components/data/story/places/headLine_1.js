import React from 'react';
import imgTwo from '../../../static/img/place/1.png'

const h_one = {
  headline: 'De Bonte Koe',
  tag: 'A gem for a bar, with a sliding door',
  img: '../img/place/1.png',
      imgtwo: {imgTwo}
};

export default h_one;
