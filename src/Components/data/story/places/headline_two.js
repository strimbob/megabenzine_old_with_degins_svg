
import React from 'react';
import imgTwo from '../../../static/img/place/2.png'

const h_Two = {
  headline: 'Hortus Botanicus',
  tag: 'Form meat eating plants, to serine gardens ',
  img: '../img/place/2.png',
  imgtwo: {imgTwo}

};

export default h_Two;
