import React from 'react';
import imgTwo from '../../../static/img/eat/1.png'
const h_one = {
  headline: 'Fromagerie Bon',
  tag: 'Phenomenal cheese and wine pairings',
  img: '../img/eat/1.png',
    imgtwo: {imgTwo}
};

export default h_one;
