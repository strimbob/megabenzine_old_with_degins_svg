import phone from '../../../static/stuffToDo/stuffToDoPhone.svg'
import desktop from '../../../static/stuffToDo/stuffToDoDtop.svg'

const stufftodo = {title: 'stuff to do',
svgPhone: phone,
svgDtop: desktop,
db: 'content/Do',
color: "yellow",
classNameTitle : "stufftodo__title yellow",
 classNames : [
   {_class: "story  story__doLeft"},
   {_class: "story story__doRight"},
   {_class: "story story__doBottomD"}]
} ;
export default stufftodo;
