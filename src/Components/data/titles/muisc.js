import phone from '../../../static/muisc/muiscPhone.svg'
import desktop from '../../../static/muisc/muiscDtop.svg'
const muisc = {title: 'muisc ',
svgPhone: phone,
svgDtop: desktop,
db: 'content/Music',
color: "pink",
classNameTitle :'muisc__title pink',
 classNames : [
   {_class: "story story__muiscLeft"},
   {_class: "story story__muiscRight"},
   {_class: "story story__muiscLeftB"}]
 };
export default muisc;
