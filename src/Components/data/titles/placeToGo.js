import phone from '../../../static/placeToGo/placeToGoPhone.svg'
import desktop from '../../../static/placeToGo/placeToGoDtop.svg'
const placeToGo = {title: 'places to go ',
svgPhone: phone,
svgDtop: desktop,
db: 'content/Place',
color: "pink",
classNameTitle :'placeToGo__title yellow',
 classNames : [
   {_class: "story story__goLeft"},
   {_class: "story story__goRight"},
   {_class: "story story__goLeftB"},
 {_class: "story story__goRightB"}
 ]
 };
export default placeToGo;
