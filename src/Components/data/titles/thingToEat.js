import phone from '../../../static/thingToEat/thingToEatPhone.svg'
import desktop from '../../../static/thingToEat/thingToEatDtop.svg'
const thingToEat = {title: 'things to eat',
svgPhone: phone,
svgDtop: desktop,
db: 'content/Eat',
color: "yellow",
classNameTitle : "thingToEat__title pink",
 classNames : [
  {_class: 'story   story__eatLeft'},
  {_class: 'story   story__eatRight'}]
};
export default thingToEat;
