require('./scss/screen.scss');
import React from 'react';
import {render} from 'react-dom';
import stuffToDo from './Components/data/titles/stuffToDo.js'
import { BrowserRouter , Switch, Router, Route ,Redirect} from 'react-router-dom'
import App from './Components/index.jsx';

import UploadStory from './Components/uploadStory.js'
import "../node_modules/normalize-scss/sass/_normalize.scss"
import ContentPage from './Components/frontPage/contentPage.js'
import SubIndex from './Components/subIndex.js'
import menuTitles from './Components/data/titles/menubar.js'
import thingToEat from './Components/data/titles/thingToEat.js'
import placeToGo from './Components/data/titles/placeToGo.js'
import muisc from './Components/data/titles/muisc.js'




render((
    <BrowserRouter basename={process.env.PUBLIC_URL}>
    <Switch>
      <Route path='/Do' render={props => <ContentPage {...props}  /> } />
      <Route path='/Eat' render={props => <ContentPage {...props}  /> } />
      <Route path='/Place' render={props => <ContentPage {...props}  /> } />
      <Route path='/Music' render={props => <ContentPage {...props}  /> } />

      <Route exact path={menuTitles[0].link}   render={props => <SubIndex {...stuffToDo} /> }  />
      <Route exact path={menuTitles[1].link}   render={props => <SubIndex {...thingToEat} /> }  />
      <Route exact path={menuTitles[2].link}   render={props => <SubIndex {...placeToGo} /> }  />
      <Route exact path={menuTitles[3].link}   render={props => <SubIndex {...muisc} /> }  />
      <Route exact path="/upload" component={UploadStory}/>
      <Route exact path="/"       component={App}   />
    </Switch>
    </BrowserRouter>
),document.getElementById('app'));
