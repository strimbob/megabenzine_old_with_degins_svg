var HtmlWebpackPlugin = require('html-webpack-plugin')
var ExtractTextPlugin = require('extract-text-webpack-plugin')
var webpack = require('webpack')
var path = require('path')
var isProd = process.env.NODE_ENV === 'production' // true or false
var cssDev = ['style-loader', 'css-loader', 'sass-loader']
var cssProd = ExtractTextPlugin.extract({
  fallback: 'style-loader',
  loader: ['css-loader', 'sass-loader'],
  publicPath: '/dist'
})
var cssConfig = isProd ? cssProd : cssDev
module.exports = {
  devtool: 'inline-source-map',
  // devtool: 'cheap-module-source-map',


  entry: {
    app: './src/app.js'
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].bundle.js'
  },
  resolve:{
    extensions: [' ', '.js', '.jsx'],
       modules: [
           'node_modules'
       ]

  },
  module: {
    loaders:[
  	{ test: /\.scss$/, loader: 'style-loader!css-loader!sass-loader'},
  	{ test: /\.js|jsx$/, exclude: /node_modules/, loaders:[ 'babel-loader?presets[]=react,presets[]=es2015']},
  	{ test: /\.(eot|ttf|woff|woff2)$/,loader: 'file-loader?name=fonts/[name].[ext]'},
    { test: /\.(jpe?g|png|gif)$/i,
      loaders: ['file-loader?name=[path][name].[ext]&outputPath=images/', {
      loader: 'image-webpack-loader',
      query: {mozjpeg: {progressive: true,},
          gifsicle: {interlaced: false,},
          optipng: {optimizationLevel: 4,},
          pngquant: {quality: '75-90',speed: 3,},},}],
      exclude: /node_modules/,include: __dirname,},
    { test: /\.svg$/,
      loader: 'babel-loader!svg-react-loader'
    }
  	]
  },
  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    compress: true,
    hot: true,
    open: true
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: 'MegaBenZine',
      hash: true,
      template: './src/html/index.html',
      minify   : {
  html5                          : true,
  collapseWhitespace             : true,
  minifyCSS                      : true,
  minifyJS                       : true,
  minifyURLs                     : false,
  removeAttributeQuotes          : true,
  removeComments                 : true,
  removeEmptyAttributes          : true,
  removeOptionalTags             : true,
  removeRedundantAttributes      : true,
  removeScriptTypeAttributes     : true,
  removeStyleLinkTypeAttributese : true,
  useShortDoctype                : true
}
    }),
    new ExtractTextPlugin({
      filename: 'app.css',
      disable: !isProd,
      allChunks: true
    }),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin()
  ]
}
